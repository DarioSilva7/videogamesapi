import { BaseEntity, Column, CreateDateColumn, Decimal128, Entity, JoinTable, ManyToMany, OneToMany, PrimaryGeneratedColumn, Unique, UpdateDateColumn } from "typeorm";
import { IsDecimal, IsNotEmpty, IsString } from "class-validator";
import { Genre } from "./Genre";
import { Platform } from "./Platform";

@Entity({
    orderBy: {
        name: "ASC",
    },
})
@Unique(["name", "id"])
export class Videogame extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    name: string;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    description: string;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    released: string;

    @Column({ default: process.env.IMAGE_GAME_DEFAULT })
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    image: string;

    @Column({ type: "numeric", precision: 2, scale: 1, nullable: false })
    @IsDecimal({ force_decimal: true, decimal_digits: '1,1' })
    @IsNotEmpty({ message: "El dato es requerido" })
    rating: number;

    @ManyToMany(() => Genre)
    @JoinTable()
    genre: Genre[]

    @ManyToMany(() => Platform)
    @JoinTable()
    platform: Platform[]

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}