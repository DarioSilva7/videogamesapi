import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryColumn, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { IsNotEmpty, IsNumber, IsString } from "class-validator";

@Entity()
export class Platform extends BaseEntity {
    @PrimaryColumn()
    @IsNumber({ allowNaN: false })
    @IsNotEmpty({ message: "El dato es requerido" })
    id: number;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    name: string;

    @Column()
    @IsNumber({ allowNaN: false })
    @IsNotEmpty({ message: "El dato es requerido" })
    games_count: number;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    icon: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}