import { BaseEntity, Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";
import { IsNotEmpty, IsString } from "class-validator";

@Entity()
export class User extends BaseEntity {
    @PrimaryGeneratedColumn("uuid")
    id: string;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    first_name: string;

    @Column()
    @IsString({ message: "El dato ingresado es incorrecto" })
    @IsNotEmpty({ message: "El dato es requerido" })
    last_name: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;
}