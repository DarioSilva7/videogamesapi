import { User } from "../../entities/User";

const CreateUserService = async (first_name: string, last_name: string) => {
    const user = User.create({ first_name, last_name })
    return user.save()
}

export default {
    CreateUserService
}