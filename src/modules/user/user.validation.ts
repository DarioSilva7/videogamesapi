import { Request, Response, NextFunction } from "express";
import { User } from "../../entities/User";
import { validate } from "class-validator";

const CreateUser = async (req: Request, res: Response, next: NextFunction) => {
    const { first_name, last_name, } = req.body;
    // if (!first_name || !last_name)
    //     next(new Error("Datos incompletos."))
    // if (typeof first_name != "string" || typeof last_name != "string")
    //     next(new Error("El tipo de dato es incorrecto."))

    const user = User.create({ first_name, last_name });
    validate(user).then(errors => {
        if (errors.length > 0) {
            next(errors)
        } else {
            User.find({ where: { first_name } }).then(founded => {
                console.log("🚀 ~ file: user.validation.ts:18 ~ User.find ~ allUsers:", founded)
                founded.length > 0 ? next(new Error(`No es posible crear un usuario con el campo first_name: ${first_name}`)) : next()
            })
        }
    })
}

export default { CreateUser };