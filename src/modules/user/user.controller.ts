import { Request, Response, NextFunction } from "express";
import userServices from "./user.service";
import { DTO_USER_CREATE } from "./user.DTO";


const CreateUser = async (req: Request, res: Response, next: NextFunction) => {
    try {

        const { first_name, last_name }: DTO_USER_CREATE = req.body;
        const userCreated = await userServices.CreateUserService(first_name, last_name)
        res.status(201).json({ msg: "Usuario creado", data: userCreated, error: [] })
    } catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: user.controller.ts:12 ~ CreateUser ~ error:", Object.keys(error))
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] })
        }
    }
}

export default { CreateUser }