import { Router } from "express";
const userRoutes = Router();
import userControllers from "./user.controller";
import userValidations from "./user.validation"


userRoutes.post("/create", userValidations.CreateUser, userControllers.CreateUser)

export default userRoutes;