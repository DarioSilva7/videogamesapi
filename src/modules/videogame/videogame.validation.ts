import { Request, Response, NextFunction } from "express";
import { Videogame } from "../../entities/Videogame";
import { validate } from "class-validator";

function validateRatingFormat(rating: string): boolean {
    // Utilizamos una expresión regular para verificar el formato: un dígito a la izquierda de la coma y un dígito a la derecha de la coma.
    const ratingPattern = /^[0-9]\.[0-9]$/;
    return ratingPattern.test(rating);
}

const CreateVideogame = async (req: Request, res: Response, next: NextFunction) => {
    const { name, description, released, image, rating, genre, platform } = req.body;
    // if (!name || !description)
    //     next(new Error("Datos incompletos."))

    const videogame = Videogame.create({ name, description, released, image, rating, genre, platform });
    validate(videogame).then(errors => {
        if (errors.length > 0) {
            next(errors)
        } else {
            !validateRatingFormat(rating) && next(new Error("El rating debe responder al formato 'number.number' por ejemplo: '5.0'"))
            // Asigna "rating" al objeto que se guardará en la base de datos
            Videogame.find({ where: { name } }).then(exists => {
                console.log("🚀 ~ file: videogame.validation.ts:16 ~ Videogame.find ~ exists:", exists)
                exists.length > 0 ? next(new Error(`No es posible crear el juego con el campo name: ${name}`)) : next()
            })
        }
    })
}

export default { CreateVideogame };