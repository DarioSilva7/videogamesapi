import { Request, Response, NextFunction } from "express";
import videogameServices from "./videogame.service";
import { DTO_VIDEOGAME_CREATE } from "./videogame.DTO";


const CreateVideogame = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { name, description, released, image, rating, genre, platform }: DTO_VIDEOGAME_CREATE = req.body;
        const videogameCreated = await videogameServices.CreateVideogameService(name, description, released, image, Number(rating), genre, platform)
        res.status(201).json({ msg: "Juego creado", data: videogameCreated, error: [] })
    } catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: videogame.controller.ts:13 ~ CreateVideogame ~ error:", Object.keys(error))
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] })
        }
    }
}

const GetAllGames = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { name } = req.query;
        let videogamesFound;
        if (typeof name == "string") videogamesFound = await videogameServices.GetAllGamesService(name)
        else videogamesFound = await videogameServices.GetAllGamesService()
        res.status(200).json({ msg: "Juego/s encontrado/s", data: videogamesFound, error: [] })
    } catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: videogame.controller.ts:29 ~ GetAllGames ~ error:", error)
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] })
        }
    }
}

const GetGameById = async (req: Request, res: Response, next: NextFunction) => {
    try {
        const { id } = req.params;
        const videogameFounded = await videogameServices.GetGameByIdService(id)
        res.status(200).json({ msg: "Juego/s encontrado/s", data: videogameFounded, error: [] })
    } catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: videogame.controller.ts:29 ~ GetGameById ~ error:", error)
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] })
        }
    }
}

export default { CreateVideogame, GetAllGames, GetGameById }