import { Genre } from "../../entities/Genre";
import { Platform } from "../../entities/Platform";

export interface DTO_VIDEOGAME_CREATE {
    name: string,
    description: string,
    released: string,
    image: string,
    rating: string,
    genre: [number],
    platform: [number]
}

