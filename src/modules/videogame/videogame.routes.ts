import { Router } from "express";
import videogameControllers from "./videogame.controller";
import videogameValidations from "./videogame.validation"
const videogameRoutes = Router();


videogameRoutes.post("/", videogameValidations.CreateVideogame, videogameControllers.CreateVideogame)
videogameRoutes.get("/:id", videogameControllers.GetGameById)
videogameRoutes.get("/videogames", videogameControllers.GetAllGames)

export default videogameRoutes;