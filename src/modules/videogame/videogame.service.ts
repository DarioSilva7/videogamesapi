import { ILike, In } from "typeorm";
import config from "../../config";
import { Genre } from "../../entities/Genre";
import { Videogame } from "../../entities/Videogame";
import axios from "axios";
import { Platform } from "../../entities/Platform";

const CreateVideogameService = async (name: string, description: string, released: string, image: string, rating: number, genre: [number], platform: [number]): Promise<Videogame> => {
    const newVideogame = Videogame.create({ name, description, released, image, rating })
    console.log("🚀 ~ file: videogame.service.ts:10 ~ CreateVideogameService ~ newVideogame:", newVideogame)
    const [genreToAdd, platformToAdd] = await Promise.all([
        Genre.find({ where: { id: In(genre) }, select: ["id", "name", "games_count"] }),
        Platform.find({ where: { id: In(platform) }, select: ["id", "name", "games_count"] })
    ])
    console.log("🚀 ~ file: videogame.service.ts:15 ~ CreateVideogameService ~ genreToAdd, platformToAdd:", genreToAdd, platformToAdd)
    newVideogame.genre = genreToAdd;
    newVideogame.platform = platformToAdd;
    console.log("🚀 ~ file: videogame.service.ts:18 ~ CreateVideogameService ~ newVideogame:", newVideogame)
    newVideogame.save()
    console.log("🚀 ~ file: videogame.service.ts:20 ~ CreateVideogameService ~ newVideogame:", newVideogame)
    return newVideogame;
}

const GetAllGamesService = async (name?: string): Promise<Videogame[]> => {
    let whereClausure: any = {}; // agregar genre platform
    let allDataSources: Videogame[];
    if (name) {
        const [dbVideogames, { data }]: any = await Promise.all([
            Videogame.find({
                where: {
                    name: ILike(`%${name}%`)
                },
                relations: {
                    genre: true,
                    platform: true
                }
            }),
            axios.get(`${config.api.url}/games?key=${config.api.key}&search=${name}`)
        ])

        allDataSources = dbVideogames.concat(mapGames(data.results));
        return allDataSources
    } else {
        const gamesDB = await Videogame.find({
            where: whereClausure,
            relations: {
                genre: true,
                platform: true
            }
        });
        const pages = [1, 2, 3, 4, 5]
        const arrayAPIVideogamesPromise = pages.map(page => {
            return axios.get(`${config.api.url}/games?key=${config.api.key}&page=${page}`)
        })
        const arrayAPIVideogames = await Promise.all(arrayAPIVideogamesPromise)
        let allVideoGames: Videogame[] = [];
        for (let i = 0; i < arrayAPIVideogames.length; i++) {
            allVideoGames = gamesDB.concat(mapGames(arrayAPIVideogames[i].data.results))
        }
        return allVideoGames
    }
}

const mapGames = (results: any): any => {
    return results.map((game: any) => {
        return {
            id: game.id,
            name: game.name,
            image: game.background_image,
            platform: game.platforms.map((obj: any) => { return { id: obj.platform.id, name: obj.platform.name, games_count: obj.platform.games_count } }),
            released: game.released,
            rating: game.rating,
            genre: game.genres.map((obj: any) => { return { id: obj.id, name: obj.name, games_count: obj.games_count } }),
        }
    })
}

const GetGameByIdService = async (id: string) => {
    let videogameFounded: any;
    if (!isNaN(parseInt(id))) {
        videogameFounded = await axios.get(`${config.api.url}/games/${id}?key=${config.api.key}`)
        if (videogameFounded) {
            return {
                id: videogameFounded.data.id,
                name: videogameFounded.data.name,
                image: videogameFounded.data.background_image,
                description: videogameFounded.data.description,
                rating: videogameFounded.data.rating,
                genre: videogameFounded.data.genres.map((g: any) => { return { id: g.id, name: g.name, games_count: g.games_count } }),
                platform: videogameFounded.data.platforms.map((obj: any) => { return { id: obj.platform.id, name: obj.platform.name, games_count: obj.platform.games_count } }),

            }
        }
    } else {
        // find into database
        videogameFounded = await Videogame.find({
            where: { id: id }, relations: {
                genre: true,
                platform: true,
            }
        })
        if (!videogameFounded) throw new Error("No se encontro en la base de datos")
        return videogameFounded
    }
}

export default {
    CreateVideogameService,
    GetAllGamesService,
    GetGameByIdService
}