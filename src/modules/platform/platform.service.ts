import axios from "axios";
import { Platform } from "../../entities/Platform";
import config from "../../config";

export const loadPlatforms = async (): Promise<void> => {
    const { data } = await axios.get(`${config.api.url}/platforms?key=${config.api.key}`)
    const arrayPlatformsPromise = data.results.map((p: any) => {
        Platform.create({
            id: p.id,
            name: p.name,
            games_count: p.games_count,
            icon: p.slug
        }).save()
    })
    await Promise.all(arrayPlatformsPromise)
    return
}