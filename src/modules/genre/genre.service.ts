import axios from "axios";
import { Genre } from "../../entities/Genre";
import config from "../../config";

export const loadGenres = async (): Promise<void> => {
    const { data } = await axios.get(`${config.api.url}/genres?key=${config.api.key}`)
    const arrayGenrePromise = data.results.map((g: any) => {
        Genre.create({
            id: g.id,
            name: g.name,
            games_count: g.games_count
        }).save()
    })
    const algo = await Promise.all(arrayGenrePromise)
    return
}