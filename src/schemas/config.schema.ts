import Joi from 'joi';

const configSchema = Joi.object({
    NODE_ENV: Joi.string()
        .allow('development')
        .allow('testing')
        .allow('production')
        .default('development'),
    // JWT_KEY: Joi.string().required(),
    SERVER_PORT: Joi.string().required(),
    DB_NAME: Joi.string().required(),
    DB_HOST: Joi.string().required(),
    DB_PORT: Joi.string().required(),
    DB_PASSWORD: Joi.string().required(),
    DB_USER: Joi.string().required(),
    URL_DB: Joi.string().required(),
    API_KEY: Joi.string().required(),
    API_URL: Joi.string().required()
    // CORS_ORIGIN: Joi.string(),
})
    .unknown()
    .required();

export default configSchema;
