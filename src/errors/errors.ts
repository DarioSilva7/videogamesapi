import { ValidationError } from "class-validator";

export class ValidationErrorResponse {
    constructor(public property: string, public value: any, public constraints: Record<string, any>) { }
}