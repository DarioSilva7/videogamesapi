import { ValidationError } from "class-validator";
import { NextFunction, Request, Response } from "express";
import { ValidationErrorResponse } from "../errors/errors";

export const handlesError = async (error: unknown, req: Request, res: Response, next: NextFunction) => {
    if (Array.isArray(error) && error[0] instanceof ValidationError) {
        const arrayError: any = [];
        for (const err of error) {
            const { property, value, constraints } = err
            arrayError.push({
                "error": "ValidationError",
                "message": `${Object.values(constraints)}. ${[property]}: ${value}`
            });
        }
        return res.status(400).json({
            ok: false,
            message: "Algo salio mal",
            data: {},
            error: arrayError
        });
    } else if (error instanceof Error) {
        return res.status(400).json({
            ok: false,
            message: "Bad request",
            data: {},
            error: [{ error: error.name, message: error.message }],
        });
    }
}