import express from "express";
import morgan from "morgan";
import cors from "cors";
import userRoutes from "./modules/user/user.routes";
import { handlesError } from "./middlewares/handlesError";
import videogameRoutes from "./modules/videogame/videogame.routes";

const app = express();

app.use(morgan("dev"))
app.use(cors())
app.use(express.json())

app.get("/ping", (req, res) => {
    res.json({ msj: "PONG" })
})

app.use("/user", userRoutes)
app.use("/videogame", videogameRoutes)
app.use(handlesError)

export default app;