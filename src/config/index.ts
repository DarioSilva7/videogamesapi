import pathLib from 'path';
import dotenv from 'dotenv';
import configSchema from '../schemas/config.schema';

const path = pathLib.join(process.cwd(), '.env');

dotenv.config({
    path//: process.env.NODE_ENV == "development" ? ".env" : ".env.prod",
})

const { error, value: envVars } = configSchema.validate(process.env);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}

export default {
    db: {
        name: envVars.DB_NAME,
        host: envVars.DB_HOST,
        port: Number(envVars.DB_PORT),
        password: envVars.DB_PASSWORD,
        user: envVars.DB_USER,
        url_db: envVars.URL_DB
    },
    server: {
        port: process.env.SERVER_PORT
    },
    api: {
        key: envVars.API_KEY,
        url: envVars.API_URL
    },
}
