
import { DataSource } from "typeorm";
import config from "./index";
import { User } from "../entities/User";
import { Videogame } from "../entities/Videogame";
import { Platform } from "../entities/Platform";
import { Genre } from "../entities/Genre";
import { loadGenres } from "../modules/genre/genre.service";
import { loadPlatforms } from "../modules/platform/platform.service";

console.log("🚀 ~ file: database.ts:4 ~ config:", config.db)

export const AppDataSource = new DataSource({
    type: "postgres",
    host: config.db.host,
    port: config.db.port,
    username: config.db.user,
    password: config.db.password,
    database: config.db.name,
    entities: [User, Videogame, Platform, Genre],
    synchronize: true,
    logging: true,
    ssl: true // apagar para local
})

export const preloadDatabase = async (): Promise<[void, void]> => {
    return await Promise.all([loadGenres(), loadPlatforms()]);
}

export const verifyDataBase = async (): Promise<string> => {
    const [genres, platforms]: any = await Promise.all([Genre.count(), Platform.count()])
    console.log("🚀 ~ file: database.ts:29 ~ verifyDataBase ~ genres, platforms:", genres, platforms)
    if (genres > 0 && platforms > 0) {
        return "DB already preload"
    } else {
        await preloadDatabase()
        return "DB preload"
    }
}