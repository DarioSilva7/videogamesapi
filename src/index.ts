import app from "./app";
import "reflect-metadata"
import { AppDataSource, verifyDataBase } from "./config/database";
import config from "./config";

const main = async () => {
    try {
        app.listen(config.server.port, async () => {
            console.log(`Server listenning on port ${config.server.port}`)
        })
        await AppDataSource.initialize()
        console.log("Database connected!")
        // const response = await verifyDataBase()
        console.log("🚀 ~ file: index.ts:14 ~ main ~ verifyDataBase():", await verifyDataBase())
    } catch (error) {
        if (error instanceof Error)
            console.error("Ocurrió un error en las conexiones, error_name: ", error.name, "error_message: ", error.message);
    }
}

main()