"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.handlesError = void 0;
const class_validator_1 = require("class-validator");
const handlesError = (error, req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    if (Array.isArray(error) && error[0] instanceof class_validator_1.ValidationError) {
        const arrayError = [];
        for (const err of error) {
            const { property, value, constraints } = err;
            arrayError.push({
                "error": "ValidationError",
                "message": `${Object.values(constraints)}. ${[property]}: ${value}`
            });
        }
        return res.status(400).json({
            ok: false,
            message: "Algo salio mal",
            data: {},
            error: arrayError
        });
    }
    else if (error instanceof Error) {
        return res.status(400).json({
            ok: false,
            message: "Bad request",
            data: {},
            error: [{ error: error.name, message: error.message }],
        });
    }
});
exports.handlesError = handlesError;
