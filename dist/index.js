"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const app_1 = __importDefault(require("./app"));
require("reflect-metadata");
const database_1 = require("./config/database");
const config_1 = __importDefault(require("./config"));
const main = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        app_1.default.listen(config_1.default.server.port, () => __awaiter(void 0, void 0, void 0, function* () {
            console.log(`Server listenning on port ${config_1.default.server.port}`);
        }));
        yield database_1.AppDataSource.initialize();
        console.log("Database connected!");
        // const response = await verifyDataBase()
        console.log("🚀 ~ file: index.ts:14 ~ main ~ verifyDataBase():", yield (0, database_1.verifyDataBase)());
    }
    catch (error) {
        if (error instanceof Error)
            console.error("Ocurrió un error en las conexiones, error_name: ", error.name, "error_message: ", error.message);
    }
});
main();
