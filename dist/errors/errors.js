"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValidationErrorResponse = void 0;
class ValidationErrorResponse {
    constructor(property, value, constraints) {
        this.property = property;
        this.value = value;
        this.constraints = constraints;
    }
}
exports.ValidationErrorResponse = ValidationErrorResponse;
