"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Videogame = void 0;
const typeorm_1 = require("typeorm");
const class_validator_1 = require("class-validator");
const Genre_1 = require("./Genre");
const Platform_1 = require("./Platform");
let Videogame = class Videogame extends typeorm_1.BaseEntity {
};
exports.Videogame = Videogame;
__decorate([
    (0, typeorm_1.PrimaryGeneratedColumn)("uuid"),
    __metadata("design:type", String)
], Videogame.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, class_validator_1.IsString)({ message: "El dato ingresado es incorrecto" }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", String)
], Videogame.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, class_validator_1.IsString)({ message: "El dato ingresado es incorrecto" }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", String)
], Videogame.prototype, "description", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, class_validator_1.IsString)({ message: "El dato ingresado es incorrecto" }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", String)
], Videogame.prototype, "released", void 0);
__decorate([
    (0, typeorm_1.Column)({ default: process.env.IMAGE_GAME_DEFAULT }),
    (0, class_validator_1.IsString)({ message: "El dato ingresado es incorrecto" }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", String)
], Videogame.prototype, "image", void 0);
__decorate([
    (0, typeorm_1.Column)({ type: "numeric", precision: 2, scale: 1, nullable: false }),
    (0, class_validator_1.IsDecimal)({ force_decimal: true, decimal_digits: '1,1' }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", Number)
], Videogame.prototype, "rating", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => Genre_1.Genre),
    (0, typeorm_1.JoinTable)(),
    __metadata("design:type", Array)
], Videogame.prototype, "genre", void 0);
__decorate([
    (0, typeorm_1.ManyToMany)(() => Platform_1.Platform),
    (0, typeorm_1.JoinTable)(),
    __metadata("design:type", Array)
], Videogame.prototype, "platform", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Videogame.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Videogame.prototype, "updated_at", void 0);
exports.Videogame = Videogame = __decorate([
    (0, typeorm_1.Entity)({
        orderBy: {
            name: "ASC",
        },
    }),
    (0, typeorm_1.Unique)(["name", "id"])
], Videogame);
