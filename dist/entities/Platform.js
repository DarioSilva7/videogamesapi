"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Platform = void 0;
const typeorm_1 = require("typeorm");
const class_validator_1 = require("class-validator");
let Platform = class Platform extends typeorm_1.BaseEntity {
};
exports.Platform = Platform;
__decorate([
    (0, typeorm_1.PrimaryColumn)(),
    (0, class_validator_1.IsNumber)({ allowNaN: false }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", Number)
], Platform.prototype, "id", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, class_validator_1.IsString)({ message: "El dato ingresado es incorrecto" }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", String)
], Platform.prototype, "name", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, class_validator_1.IsNumber)({ allowNaN: false }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", Number)
], Platform.prototype, "games_count", void 0);
__decorate([
    (0, typeorm_1.Column)(),
    (0, class_validator_1.IsString)({ message: "El dato ingresado es incorrecto" }),
    (0, class_validator_1.IsNotEmpty)({ message: "El dato es requerido" }),
    __metadata("design:type", String)
], Platform.prototype, "icon", void 0);
__decorate([
    (0, typeorm_1.CreateDateColumn)(),
    __metadata("design:type", Date)
], Platform.prototype, "created_at", void 0);
__decorate([
    (0, typeorm_1.UpdateDateColumn)(),
    __metadata("design:type", Date)
], Platform.prototype, "updated_at", void 0);
exports.Platform = Platform = __decorate([
    (0, typeorm_1.Entity)()
], Platform);
