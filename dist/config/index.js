"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const path_1 = __importDefault(require("path"));
const dotenv_1 = __importDefault(require("dotenv"));
const config_schema_1 = __importDefault(require("../schemas/config.schema"));
const path = path_1.default.join(process.cwd(), '.env');
dotenv_1.default.config({
    path //: process.env.NODE_ENV == "development" ? ".env" : ".env.prod",
});
const { error, value: envVars } = config_schema_1.default.validate(process.env);
if (error) {
    throw new Error(`Config validation error: ${error.message}`);
}
exports.default = {
    db: {
        name: envVars.DB_NAME,
        host: envVars.DB_HOST,
        port: Number(envVars.DB_PORT),
        password: envVars.DB_PASSWORD,
        user: envVars.DB_USER,
        url_db: envVars.URL_DB
    },
    server: {
        port: process.env.SERVER_PORT
    },
    api: {
        key: envVars.API_KEY,
        url: envVars.API_URL
    },
};
