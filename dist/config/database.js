"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.verifyDataBase = exports.preloadDatabase = exports.AppDataSource = void 0;
const typeorm_1 = require("typeorm");
const index_1 = __importDefault(require("./index"));
const User_1 = require("../entities/User");
const Videogame_1 = require("../entities/Videogame");
const Platform_1 = require("../entities/Platform");
const Genre_1 = require("../entities/Genre");
const genre_service_1 = require("../modules/genre/genre.service");
const platform_service_1 = require("../modules/platform/platform.service");
console.log("🚀 ~ file: database.ts:4 ~ config:", index_1.default.db);
exports.AppDataSource = new typeorm_1.DataSource({
    type: "postgres",
    host: index_1.default.db.host,
    port: index_1.default.db.port,
    username: index_1.default.db.user,
    password: index_1.default.db.password,
    database: index_1.default.db.name,
    entities: [User_1.User, Videogame_1.Videogame, Platform_1.Platform, Genre_1.Genre],
    synchronize: true,
    logging: true,
    ssl: true // apagar para local
});
const preloadDatabase = () => __awaiter(void 0, void 0, void 0, function* () {
    return yield Promise.all([(0, genre_service_1.loadGenres)(), (0, platform_service_1.loadPlatforms)()]);
});
exports.preloadDatabase = preloadDatabase;
const verifyDataBase = () => __awaiter(void 0, void 0, void 0, function* () {
    const [genres, platforms] = yield Promise.all([Genre_1.Genre.count(), Platform_1.Platform.count()]);
    console.log("🚀 ~ file: database.ts:29 ~ verifyDataBase ~ genres, platforms:", genres, platforms);
    if (genres > 0 && platforms > 0) {
        return "DB already preload";
    }
    else {
        yield (0, exports.preloadDatabase)();
        return "DB preload";
    }
});
exports.verifyDataBase = verifyDataBase;
