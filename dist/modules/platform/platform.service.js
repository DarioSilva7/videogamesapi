"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadPlatforms = void 0;
const axios_1 = __importDefault(require("axios"));
const Platform_1 = require("../../entities/Platform");
const config_1 = __importDefault(require("../../config"));
const loadPlatforms = () => __awaiter(void 0, void 0, void 0, function* () {
    const { data } = yield axios_1.default.get(`${config_1.default.api.url}/platforms?key=${config_1.default.api.key}`);
    const arrayPlatformsPromise = data.results.map((p) => {
        Platform_1.Platform.create({
            id: p.id,
            name: p.name,
            games_count: p.games_count,
            icon: p.slug
        }).save();
    });
    yield Promise.all(arrayPlatformsPromise);
    return;
});
exports.loadPlatforms = loadPlatforms;
