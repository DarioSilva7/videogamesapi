"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const videogame_controller_1 = __importDefault(require("./videogame.controller"));
const videogame_validation_1 = __importDefault(require("./videogame.validation"));
const videogameRoutes = (0, express_1.Router)();
videogameRoutes.post("/", videogame_validation_1.default.CreateVideogame, videogame_controller_1.default.CreateVideogame);
videogameRoutes.get("/:id", videogame_controller_1.default.GetGameById);
videogameRoutes.get("/videogames", videogame_controller_1.default.GetAllGames);
exports.default = videogameRoutes;
