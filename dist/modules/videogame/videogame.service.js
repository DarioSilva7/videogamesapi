"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
const config_1 = __importDefault(require("../../config"));
const Genre_1 = require("../../entities/Genre");
const Videogame_1 = require("../../entities/Videogame");
const axios_1 = __importDefault(require("axios"));
const Platform_1 = require("../../entities/Platform");
const CreateVideogameService = (name, description, released, image, rating, genre, platform) => __awaiter(void 0, void 0, void 0, function* () {
    const newVideogame = Videogame_1.Videogame.create({ name, description, released, image, rating });
    console.log("🚀 ~ file: videogame.service.ts:10 ~ CreateVideogameService ~ newVideogame:", newVideogame);
    const [genreToAdd, platformToAdd] = yield Promise.all([
        Genre_1.Genre.find({ where: { id: (0, typeorm_1.In)(genre) }, select: ["id", "name", "games_count"] }),
        Platform_1.Platform.find({ where: { id: (0, typeorm_1.In)(platform) }, select: ["id", "name", "games_count"] })
    ]);
    console.log("🚀 ~ file: videogame.service.ts:15 ~ CreateVideogameService ~ genreToAdd, platformToAdd:", genreToAdd, platformToAdd);
    newVideogame.genre = genreToAdd;
    newVideogame.platform = platformToAdd;
    console.log("🚀 ~ file: videogame.service.ts:18 ~ CreateVideogameService ~ newVideogame:", newVideogame);
    newVideogame.save();
    console.log("🚀 ~ file: videogame.service.ts:20 ~ CreateVideogameService ~ newVideogame:", newVideogame);
    return newVideogame;
});
const GetAllGamesService = (name) => __awaiter(void 0, void 0, void 0, function* () {
    let whereClausure = {}; // agregar genre platform
    let allDataSources;
    if (name) {
        const [dbVideogames, { data }] = yield Promise.all([
            Videogame_1.Videogame.find({
                where: {
                    name: (0, typeorm_1.ILike)(`%${name}%`)
                },
                relations: {
                    genre: true,
                    platform: true
                }
            }),
            axios_1.default.get(`${config_1.default.api.url}/games?key=${config_1.default.api.key}&search=${name}`)
        ]);
        allDataSources = dbVideogames.concat(mapGames(data.results));
        return allDataSources;
    }
    else {
        const gamesDB = yield Videogame_1.Videogame.find({
            where: whereClausure,
            relations: {
                genre: true,
                platform: true
            }
        });
        const pages = [1, 2, 3, 4, 5];
        const arrayAPIVideogamesPromise = pages.map(page => {
            return axios_1.default.get(`${config_1.default.api.url}/games?key=${config_1.default.api.key}&page=${page}`);
        });
        const arrayAPIVideogames = yield Promise.all(arrayAPIVideogamesPromise);
        let allVideoGames = [];
        for (let i = 0; i < arrayAPIVideogames.length; i++) {
            allVideoGames = gamesDB.concat(mapGames(arrayAPIVideogames[i].data.results));
        }
        return allVideoGames;
    }
});
const mapGames = (results) => {
    return results.map((game) => {
        return {
            id: game.id,
            name: game.name,
            image: game.background_image,
            platform: game.platforms.map((obj) => { return { id: obj.platform.id, name: obj.platform.name, games_count: obj.platform.games_count }; }),
            released: game.released,
            rating: game.rating,
            genre: game.genres.map((obj) => { return { id: obj.id, name: obj.name, games_count: obj.games_count }; }),
        };
    });
};
const GetGameByIdService = (id) => __awaiter(void 0, void 0, void 0, function* () {
    let videogameFounded;
    if (!isNaN(parseInt(id))) {
        videogameFounded = yield axios_1.default.get(`${config_1.default.api.url}/games/${id}?key=${config_1.default.api.key}`);
        if (videogameFounded) {
            return {
                id: videogameFounded.data.id,
                name: videogameFounded.data.name,
                image: videogameFounded.data.background_image,
                description: videogameFounded.data.description,
                rating: videogameFounded.data.rating,
                genre: videogameFounded.data.genres.map((g) => { return { id: g.id, name: g.name, games_count: g.games_count }; }),
                platform: videogameFounded.data.platforms.map((obj) => { return { id: obj.platform.id, name: obj.platform.name, games_count: obj.platform.games_count }; }),
            };
        }
    }
    else {
        // find into database
        videogameFounded = yield Videogame_1.Videogame.find({
            where: { id: id }, relations: {
                genre: true,
                platform: true,
            }
        });
        if (!videogameFounded)
            throw new Error("No se encontro en la base de datos");
        return videogameFounded;
    }
});
exports.default = {
    CreateVideogameService,
    GetAllGamesService,
    GetGameByIdService
};
