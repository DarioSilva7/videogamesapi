"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Videogame_1 = require("../../entities/Videogame");
const class_validator_1 = require("class-validator");
function validateRatingFormat(rating) {
    // Utilizamos una expresión regular para verificar el formato: un dígito a la izquierda de la coma y un dígito a la derecha de la coma.
    const ratingPattern = /^[0-9]\.[0-9]$/;
    return ratingPattern.test(rating);
}
const CreateVideogame = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { name, description, released, image, rating, genre, platform } = req.body;
    // if (!name || !description)
    //     next(new Error("Datos incompletos."))
    const videogame = Videogame_1.Videogame.create({ name, description, released, image, rating, genre, platform });
    (0, class_validator_1.validate)(videogame).then(errors => {
        if (errors.length > 0) {
            next(errors);
        }
        else {
            !validateRatingFormat(rating) && next(new Error("El rating debe responder al formato 'number.number' por ejemplo: '5.0'"));
            // Asigna "rating" al objeto que se guardará en la base de datos
            Videogame_1.Videogame.find({ where: { name } }).then(exists => {
                console.log("🚀 ~ file: videogame.validation.ts:16 ~ Videogame.find ~ exists:", exists);
                exists.length > 0 ? next(new Error(`No es posible crear el juego con el campo name: ${name}`)) : next();
            });
        }
    });
});
exports.default = { CreateVideogame };
