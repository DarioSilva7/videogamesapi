"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const videogame_service_1 = __importDefault(require("./videogame.service"));
const CreateVideogame = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name, description, released, image, rating, genre, platform } = req.body;
        const videogameCreated = yield videogame_service_1.default.CreateVideogameService(name, description, released, image, Number(rating), genre, platform);
        res.status(201).json({ msg: "Juego creado", data: videogameCreated, error: [] });
    }
    catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: videogame.controller.ts:13 ~ CreateVideogame ~ error:", Object.keys(error));
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] });
        }
    }
});
const GetAllGames = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { name } = req.query;
        let videogamesFound;
        if (typeof name == "string")
            videogamesFound = yield videogame_service_1.default.GetAllGamesService(name);
        else
            videogamesFound = yield videogame_service_1.default.GetAllGamesService();
        res.status(200).json({ msg: "Juego/s encontrado/s", data: videogamesFound, error: [] });
    }
    catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: videogame.controller.ts:29 ~ GetAllGames ~ error:", error);
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] });
        }
    }
});
const GetGameById = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const videogameFounded = yield videogame_service_1.default.GetGameByIdService(id);
        res.status(200).json({ msg: "Juego/s encontrado/s", data: videogameFounded, error: [] });
    }
    catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: videogame.controller.ts:29 ~ GetGameById ~ error:", error);
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] });
        }
    }
});
exports.default = { CreateVideogame, GetAllGames, GetGameById };
