"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const User_1 = require("../../entities/User");
const class_validator_1 = require("class-validator");
const CreateUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const { first_name, last_name, } = req.body;
    // if (!first_name || !last_name)
    //     next(new Error("Datos incompletos."))
    // if (typeof first_name != "string" || typeof last_name != "string")
    //     next(new Error("El tipo de dato es incorrecto."))
    const user = User_1.User.create({ first_name, last_name });
    (0, class_validator_1.validate)(user).then(errors => {
        if (errors.length > 0) {
            next(errors);
        }
        else {
            User_1.User.find({ where: { first_name } }).then(founded => {
                console.log("🚀 ~ file: user.validation.ts:18 ~ User.find ~ allUsers:", founded);
                founded.length > 0 ? next(new Error(`No es posible crear un usuario con el campo first_name: ${first_name}`)) : next();
            });
        }
    });
});
exports.default = { CreateUser };
