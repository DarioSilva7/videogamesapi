"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const user_service_1 = __importDefault(require("./user.service"));
const CreateUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { first_name, last_name } = req.body;
        const userCreated = yield user_service_1.default.CreateUserService(first_name, last_name);
        res.status(201).json({ msg: "Usuario creado", data: userCreated, error: [] });
    }
    catch (error) {
        if (error instanceof Error) {
            console.log("🚀 ~ file: user.controller.ts:12 ~ CreateUser ~ error:", Object.keys(error));
            res.status(500).json({ msg: error.message, stack: error.stack, error: [error.name] });
        }
    }
});
exports.default = { CreateUser };
