"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.loadGenres = void 0;
const axios_1 = __importDefault(require("axios"));
const Genre_1 = require("../../entities/Genre");
const config_1 = __importDefault(require("../../config"));
const loadGenres = () => __awaiter(void 0, void 0, void 0, function* () {
    const { data } = yield axios_1.default.get(`${config_1.default.api.url}/genres?key=${config_1.default.api.key}`);
    const arrayGenrePromise = data.results.map((g) => {
        Genre_1.Genre.create({
            id: g.id,
            name: g.name,
            games_count: g.games_count
        }).save();
    });
    const algo = yield Promise.all(arrayGenrePromise);
    return;
});
exports.loadGenres = loadGenres;
