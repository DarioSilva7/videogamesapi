"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const user_routes_1 = __importDefault(require("./modules/user/user.routes"));
const handlesError_1 = require("./middlewares/handlesError");
const videogame_routes_1 = __importDefault(require("./modules/videogame/videogame.routes"));
const app = (0, express_1.default)();
app.use((0, morgan_1.default)("dev"));
app.use((0, cors_1.default)());
app.use(express_1.default.json());
app.get("/ping", (req, res) => {
    res.json({ msj: "PONG" });
});
app.use("/user", user_routes_1.default);
app.use("/videogame", videogame_routes_1.default);
app.use(handlesError_1.handlesError);
exports.default = app;
