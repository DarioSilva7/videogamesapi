"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const joi_1 = __importDefault(require("joi"));
const configSchema = joi_1.default.object({
    NODE_ENV: joi_1.default.string()
        .allow('development')
        .allow('testing')
        .allow('production')
        .default('development'),
    // JWT_KEY: Joi.string().required(),
    SERVER_PORT: joi_1.default.string().required(),
    DB_NAME: joi_1.default.string().required(),
    DB_HOST: joi_1.default.string().required(),
    DB_PORT: joi_1.default.string().required(),
    DB_PASSWORD: joi_1.default.string().required(),
    DB_USER: joi_1.default.string().required(),
    URL_DB: joi_1.default.string().required(),
    API_KEY: joi_1.default.string().required(),
    API_URL: joi_1.default.string().required()
    // CORS_ORIGIN: Joi.string(),
})
    .unknown()
    .required();
exports.default = configSchema;
